package com.javadevmatt.jumptutorial;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Array;

public class JumpTutorial extends ApplicationAdapter {

	private Music music;
	private Texture playerTexture, platformTexture;
	private JumpPlayer player;
	private Array<Platform> platormArray;
	private OrthographicCamera camera;

	private float gravity = -20;

	private SpriteBatch batch;

	@Override
	public void create() {
		loadData();
		init();
	}

	private void loadData() {
		playerTexture = new Texture("player.png");
		platformTexture = new Texture("platform.png");
		music = Gdx.audio.newMusic(Gdx.files.internal("music.ogg"));
	}

	private void init() {
		music.play();
		batch = new SpriteBatch();
		camera = new OrthographicCamera(480, 800);

		player = new JumpPlayer(playerTexture);

		platormArray = new Array<Platform>();

		for (int i = 1; i <= 20; i++) {
			Platform p1 = new Platform(platformTexture);
			p1.x = MathUtils.random(480);
			p1.y = 200 * i;
			platormArray.add(p1);
		}

	}

	@Override
	public void render() {
		update();

		Gdx.gl.glClearColor(1, 1, 1, 0);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		batch.begin();
		batch.setProjectionMatrix(camera.combined);

		for (Platform p : platormArray) {
			p.draw(batch);
		}
		player.draw(batch);
		batch.end();
	}

	private void update() {
		handleInput();
		camera.update();
		camera.position.set(player.x + player.width / 2,
				player.y + 300, 0);

		player.y += player.jumpVelocity * Gdx.graphics.getDeltaTime();
		
		if (player.y > 0) {
			player.jumpVelocity += gravity;
		} else {
			player.y = 0;
			player.canJump = true;
			player.jumpVelocity = 0;
		}

		for (Platform p : platormArray) {
			if (isPlayerOnPlatform(p)) {
				player.canJump = true;
				player.jumpVelocity = 0;
				player.y = p.y + p.height;
			}
		}

	}

	private boolean isPlayerOnPlatform(Platform p) {
		return player.jumpVelocity <= 0 && player.overlaps(p)
				&& !(player.y <= p.y);
	}

	private void handleInput() {
		if (Gdx.input.isKeyPressed(Keys.A)) {
			player.x -= 500 * Gdx.graphics.getDeltaTime();
		}

		if (Gdx.input.isKeyPressed(Keys.D)) {
			player.x += 500 * Gdx.graphics.getDeltaTime();
		}
		if (Gdx.input.justTouched()) {
			player.jump();
		}
	}
	
}
